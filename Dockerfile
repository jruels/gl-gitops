FROM python:slim-bullseye

WORKDIR /app

COPY ./src .

EXPOSE 5000

CMD [ "python", "main.py" ]
